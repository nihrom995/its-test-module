<?php

namespace Nihrom\Seo;

use Bitrix\Main;

/**
 * Класс устанавливает заголовк и описаниею по событию
 */
class SeoSetter
{
    public static function onEpilog()
    {
        $url = Utils::getCurrentUriWithOutParams();
        $seoData = self::getSeoData($url, SITE_ID);
        if (empty($seoData)) {
            return false;
        }

        if ($seoData['TITLE'] != '') {
            self::setTitle($seoData['TITLE']);
        }

        if ($seoData['DESCRIPTION'] != '') {
            self::setDescription($seoData['DESCRIPTION']);
        }

        return true;
    }

    public static function getSeoData($url, $siteId)
    {
        return current(NihromSeoTable::getList(['filter' => ['URL' => $url, 'SITE_ID' => $siteId,]])->fetchAll());
    }

    public static function setTitle($title)
    {
        global $APPLICATION;
        $APPLICATION->SetPageProperty('title', $title);
    }

    public static function setDescription($description)
    {
        global $APPLICATION;
        $APPLICATION->SetPageProperty('description', $description);
    }
}