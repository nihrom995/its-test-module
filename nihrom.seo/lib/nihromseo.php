<?php

namespace Nihrom\Seo;

use Bitrix\Main\Entity;

class NihromSeoTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'nihrom_seo';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('URL', ['required' => true]),
            new Entity\StringField('TITLE'),
            new Entity\TextField('DESCRIPTION'),
            new Entity\StringField('SITE_ID', ['required' => true]),
        );
    }
}