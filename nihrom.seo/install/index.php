<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class nihrom_seo extends CModule
{
    public $MODULE_ID = 'nihrom.seo';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_ID = 'nihrom.seo';
        $this->MODULE_NAME = Loc::GetMessage("NIHROM_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::GetMessage("NIHROM_MODULE_DESC");
        $this->MODULE_GROUP_RIGHTS = 'N';

        $this->PARTNER_NAME = Loc::GetMessage("NIHROM_PARTNER_NAME");
        $this->PARTNER_URI = Loc::GetMessage("NIHROM_PARTNER_URI");
    }

    public function doUninstall()
    {
        global $APPLICATION, $step;
        $step = intval($step);

        if ($step < 2) {
            $APPLICATION->IncludeAdminFile(GetMessage("NIHROM_INSTALL_TITLE"), __DIR__ . "/unstep1.php");
        } elseif ($step == 2) {
            if (!$this->UnInstallDB(array(
                "savedata" => $_REQUEST["savedata"],
            ))) {
                return false;
            }

            $this->UnInstallEvents();
            $this->UnInstallFiles();

            ModuleManager::unRegisterModule($this->MODULE_ID);

            $GLOBALS["errors"] = $this->errors;

            $APPLICATION->IncludeAdminFile(GetMessage("NIHROM_INSTALL_TITLE"), __DIR__ . "/unstep2.php");
        }

        return true;
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);

        if (!$this->InstallDB()) {
            return false;
        }

        if (!$this->InstallEvents()) {
            $this->UninstallDB();
            return false;
        }

        if (!$this->InstallFiles()) {
            $this->UninstallDB(array(
                "savedata" => $_REQUEST["savedata"],
            ));
            $this->UninstallFiles();
            return false;
        }

        return true;
    }

    public function InstallDB()
    {
        Loader::includeModule($this->MODULE_ID);
        $connection = \Bitrix\Main\Application::getConnection();

        foreach ($this->getTables() as $tableClass) {
            if (!$connection->isTableExists($tableClass::getTableName())) {
                $tableClass::getEntity()->createDbTable();
            }
        }

        return true;
    }

    public function UninstallDB($arParams = array())
    {
        if (array_key_exists("savedata", $arParams) && $arParams["savedata"] != "Y") {
            Loader::includeModule($this->MODULE_ID);
            $connection = Application::getInstance()->getConnection();

            foreach ($this->getTables() as $tableClass) {
                if ($connection->isTableExists($tableClass::getTableName())) {
                    $connection->dropTable($tableClass::getTableName());
                }
            }
        }

        return true;
    }

    public function InstallEvents()
    {
        RegisterModuleDependences("main", "OnPanelCreate", $this->MODULE_ID, "\Nihrom\Seo\TopPanel", "onPanelCreate");
        RegisterModuleDependences("main", "OnEpilog", $this->MODULE_ID, "\Nihrom\Seo\SeoSetter", "onEpilog");

        CModule::IncludeModule($this->MODULE_ID);

        return true;
    }

    public function UninstallEvents()
    {
        UnRegisterModuleDependences("main", "OnPanelCreate", $this->MODULE_ID, "\Nihrom\Seo\TopPanel", "onPanelCreate");
        UnRegisterModuleDependences("main", "OnEpilog", $this->MODULE_ID, "\Nihrom\Seo\SeoSetter", "onEpilog");

        return true;
    }

    public function InstallFiles()
    {
        if ($_ENV["COMPUTERNAME"] != 'BX') {
            CopyDirFiles(__DIR__ . "/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true);
        }

        return true;
    }

    public function UninstallFiles()
    {
        if ($_ENV["COMPUTERNAME"] != 'BX') {
            DeleteDirFiles(__DIR__ . "/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        }

        return true;
    }

    /**
     * @return DataManager[]
     */
    private function getTables(): array
    {
        return [
            \Nihrom\Seo\NihromSeoTable::class
        ];
    }
}
