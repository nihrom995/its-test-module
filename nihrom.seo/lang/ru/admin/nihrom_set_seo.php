<?php

$MESS["NIHROM_POPUP_TITLE"] = "Изменение SEO данных";
$MESS["NIHROM_POPUP_PAGE_TITLE"] = "Заголовок";
$MESS["NIHROM_POPUP_PAGE_DESCRIPTION"] = "Описание";
$MESS["NIHROM_POPUP_EMPTY_SITE_ID"] = "Не задан идентификатор сайт, перезагрузите страницу и попробуйте снова.";
$MESS["NIHROM_POPUP_EMPTY_URL"] = "Не задан URL страницы. Перезагрузите страницу и попробуйте снова";
$MESS["NIHROM_SUCCESS_MESSAGE"] = "Данные успешно сохранены";