<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use \Nihrom\Seo\NihromSeoTable;
use \Nihrom\Seo\Utils;
use Bitrix\Main\Loader;

global $USER;

if(!Loader::includeModule('nihrom.seo'))
    return false;

IncludeModuleLangFile(__FILE__);

$popupWindow = new CJSPopup(GetMessage("NIHROM_POPUP_TITLE"));

//Проверка прав доступа
if (!$USER->IsAdmin() && !Utils::isUserContentManager())
    $popupWindow->ShowError(GetMessage("PAGE_PROP_ACCESS_DENIED"));


$pageUrl = (isset($_REQUEST['pageUrl']) && $_REQUEST['pageUrl'] != '') ? $_REQUEST['pageUrl'] : '';
$siteId = (isset($_REQUEST['siteId']) && $_REQUEST['siteId'] != '') ? $_REQUEST['siteId'] : '';

//Ищем СЕО данные по URL в таблице
$seoResult = current(NihromSeoTable::getList(['filter' => ['URL' => $pageUrl, 'SITE_ID' => $siteId,]])->fetchAll());


//Инициализируем переменные заголовка и описания
$seoTitle = $_POST['seoTitle'] ?? ($seoResult['TITLE'] ?? '');
$seoDescription = $_POST['seoDescription'] ?? ($seoResult['DESCRIPTION'] ?? '');

$strWarning = "";

//Сохранение формы
if ($_SERVER["REQUEST_METHOD"] == "POST" && !check_bitrix_sessid()) {
    CUtil::JSPostUnescape();
    $strWarning = GetMessage("MAIN_SESSION_EXPIRED");
} elseif ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_REQUEST["save"]) && $pageUrl && $siteId) {
    CUtil::JSPostUnescape();

    if ($seoResult !== false) {
        $result = NihromSeoTable::update($seoResult['ID'], array(
            'TITLE' => $_POST['seoTitle'],
            'DESCRIPTION' => $_POST['seoDescription'],
        ));
    } else {
        $result = NihromSeoTable::add(array(
            'URL' => $pageUrl,
            'TITLE' => $_POST['seoTitle'],
            'DESCRIPTION' => $_POST['seoDescription'],
            'SITE_ID' => $siteId
        ));
    }


    if ($result->isSuccess()) {
        $popupWindow->Close(false, '');
    } else {
        $strWarning = join(';', $result->getErrorMessages());
    }
}

//Вывод Popup
$popupWindow->ShowTitlebar(GetMessage("NIHROM_POPUP_TITLE"));
$popupWindow->StartDescription("bx-nihrom-seo");

if ($strWarning != "") {
    $popupWindow->ShowValidationError($strWarning);
}

$popupWindow->EndDescription();

$popupWindow->StartContent();
?>
    <table id="nihrom_seo" style="width: 100%">
        <tr>
            <td class="bx-popup-label bx-width30"><?= GetMessage("NIHROM_POPUP_PAGE_TITLE") ?>:</td>
            <td><input type="text" style="width:90%;" name="seoTitle" value="<?= htmlspecialcharsEx($seoTitle) ?>">
            </td>
        </tr>
        <tr>
            <td class="bx-popup-label bx-width30"><?= GetMessage("NIHROM_POPUP_PAGE_DESCRIPTION") ?>:</td>
            <td>
                <textarea rows="5" style="width:90%;" name="seoDescription"><?= htmlspecialcharsEx(
                        $seoDescription
                    ) ?></textarea>
            </td>
        </tr>
    </table>
    <input type="hidden" name="save" value="Y"/>
<?php
$popupWindow->EndContent();
$popupWindow->ShowStandardButtons();
?>
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin_js.php"); ?>