<?php

$MESS["NIHROM_SEO_TABLE_EMPTY_DATA"] = "Должно быть заполнено поле Заголовок или Описание";
$MESS["NIHROM_SEO_TABLE_EMPTY_URL"] = "Не найден URL страницы!";
$MESS["NIHROM_SEO_TABLE_BAD_URL"] = "Некорректный URL страницы!";