<?php

namespace Nihrom\Seo;

/**
 * Класс для вывода элементов управления модуля
 * в панель администратора в публичной части сайта
 */
class TopPanel
{
    public static function onPanelCreate()
    {
        global $APPLICATION, $USER;

        //Проверка прав доступа
        if (!$USER->IsAdmin() && !Utils::isUserContentManager())
            return false;

        $pageUrl = Utils::getCurrentUriWithOutParams();

        $defaultUrl = $APPLICATION->GetPopupLink([
            "URL" => "/bitrix/admin/public_nihrom_set_seo.php?siteId=" . SITE_ID . '&pageUrl=' . $pageUrl,
            "PARAMS" => ["min_width" => 450, "min_height" => 250]
        ]);

        $APPLICATION->AddPanelButton(array(
            "HREF" => ($defaultUrl == "" ? "" : "javascript:" . $defaultUrl),
            "TYPE" => "BIG",
            "SRC" => "",
            "TEXT" => GetMessage('NIHROM_TOP_PANEL_TITLE'),
            "MAIN_SORT" => 1100,
            "SORT" => 50
        ));

        return true;
    }
}