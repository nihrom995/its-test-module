<?php

namespace Nihrom\Seo;

use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\GroupTable;

class Utils
{
    public static function getCurrentUriWithOutParams(): string
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $uriString = $request->getRequestUri();
        $uri = new Uri($uriString);

        return $uri->getPath();
    }

    /*
     * Проверяем принадлежит ли текущий пользователь к группе контент менеджеров.
     * Контент менеджеры это менеджеры у которых символьный код может быть таким:
     * contentman, conentman
     */
    public static function isUserContentManager(){
        global $USER;

        $arGroups = GroupTable::GetList(['filter'=>["STRING_ID"=>['contentman','conentman']]])->fetchAll();
        $arUsersManagerGroups = [];
        if(count($arGroups) > 0)
        {
            foreach($arGroups as $group)
            {
                $arUsersManagerGroups[] = $group['ID'];
            }

            if(count(array_intersect($USER->GetUserGroupArray(), $arUsersManagerGroups)) > 0)
                return true;
        }

        return false;
    }
}